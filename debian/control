Source: golang-github-gorilla-csrf
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Anthony Fok <foka@debian.org>
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-golang
Build-Depends-Indep: golang-any,
                     golang-github-gorilla-securecookie-dev,
                     golang-github-pkg-errors-dev
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-gorilla-csrf
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-gorilla-csrf.git
Homepage: https://github.com/gorilla/csrf
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/gorilla/csrf

Package: golang-github-gorilla-csrf-dev
Architecture: all
Depends: ${misc:Depends},
         golang-github-gorilla-securecookie-dev,
         golang-github-pkg-errors-dev
Description: Cross Site Request Forgery (CSRF) prevention middleware for Go
 gorilla/csrf is a HTTP middleware library that provides cross-site request
 forgery (CSRF) protection.  It includes:
 .
  * The csrf.Protect middleware/handler provides CSRF protection on routes
    attached to a router or a sub-router.
  * A csrf.Token function that provides the token to pass into your response,
    whether that be a HTML form or a JSON response body.
  * ... and a csrf.TemplateField helper that you can pass into your
    html/template templates to replace a {{ .csrfField }} template tag
    with a hidden input field.
 .
 gorilla/csrf is designed to work with any Go web framework, including:
 .
  * The Gorilla toolkit
  * Go's built-in net/http package
  * Goji - see the tailored fork https://github.com/goji/csrf
  * Gin
  * Echo
  * ... and any other router/framework that rallies around Go's http.Handler
    interface.
 .
 gorilla/csrf is also compatible with middleware 'helper' libraries
 like Alice and Negroni.
